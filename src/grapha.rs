use std::collections::HashMap;
use std::ops::Add;

pub struct Vertex<CostType>
where
    CostType: std::cmp::Eq,
{
    pub id: usize,
    pub adjacent: HashMap<usize, CostType>,
}

pub struct Graph<CostType>
where
    CostType: std::cmp::Eq + std::fmt::Display,
{
    vertices: Vec<Vertex<CostType>>,
    largest_id: usize,
}

pub enum GraphError {
    DepartureVertexAbsent,
    DestinationVertexAbsent,
    VertexAbsent,
}

impl<CostType> Graph<CostType>
where
    CostType: std::cmp::Eq + std::fmt::Display,
{
    pub fn default() -> Self {
        Graph {
            vertices: Vec::<Vertex<CostType>>::new(),
            largest_id: 0_usize,
        }
    }

    pub fn with_capacity(size: usize) -> Self {
        Graph {
            vertices: Vec::<Vertex<CostType>>::with_capacity(size),
            largest_id: 0_usize,
        }
    }

    pub fn add_node(&mut self) -> &Vertex<CostType> {
        self.vertices.push(Vertex {
            id: self.largest_id.add(1),
            adjacent: HashMap::<usize, CostType>::new(),
        });
        self.vertices.last().unwrap()
    }

    pub fn add_edge(
        &mut self,
        (departure, destination): (usize, usize),
        cost: CostType,
    ) -> Result<(), GraphError> {
        if self
            .vertices
            .iter()
            .find(|vertex| vertex.id == destination)
            .is_none()
        {
            return Err(GraphError::DestinationVertexAbsent);
        }

        let departure_node: &mut Vertex<CostType> = match self
            .vertices
            .iter_mut()
            .find(|vertex| vertex.id == departure)
        {
            Some(vertex) => vertex,
            None => return Err(GraphError::DepartureVertexAbsent),
        };

        departure_node.adjacent.insert(destination, cost);
        Ok(())
    }

    pub fn remove_node(&mut self, id: usize) -> Result<(), GraphError> {
        let node_index: usize = match self
            .vertices
            .iter()
            .enumerate()
            .find(|(_index, vertex)| vertex.id == id)
        {
            Some((id, _)) => id,
            None => return Err(GraphError::VertexAbsent),
        };

        self.vertices.remove(node_index);
        self.vertices.iter_mut().for_each(|vertex| {
            vertex.adjacent.remove(&id);
        });
        Ok(())
    }

    pub fn is_contain(&self, id: usize) -> bool {
        self.vertices
            .iter()
            .find(|vertex| vertex.id == id)
            .is_some()
    }

    pub fn get_node(&self, id: usize) -> Result<&Vertex<CostType>, GraphError> {
        match self.vertices.iter().find(|vertex| vertex.id == id) {
            Some(vertex) => Ok(vertex),
            None => Err(GraphError::VertexAbsent),
        }
    }

    pub fn get_ids(&self) -> Vec<usize> {
        self.vertices
            .iter()
            .map(|vertex| vertex.id)
            .collect::<Vec<usize>>()
    }

    pub fn to_string(&self) -> String {
        let mut result: String = String::with_capacity(1024);
        self.vertices.iter().for_each(|vertex| {
            result += format!("ID: {}. Adjacent vertices: ", vertex.id).as_str();
            vertex
                .adjacent
                .iter()
                .for_each(|(id, cost)| result += format!("({},{}) ", id, cost).as_str());
            result += "\n"
        });

        result.shrink_to_fit();
        result
    }
}
